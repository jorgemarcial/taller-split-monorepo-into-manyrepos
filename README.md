# Split monorepo into manyrepos #

### Dividir monorepositorio de un proyecto multiaplicación en multiples repos. ###

* Bajar instalador de [symfony multiaplicación](https://github.com/bcastellano/symfony-installer) si es que no lo tenemos.
* Creamos los siguientes repositorios:
* splitter-project
* splitter-cms-bundle
* splitter-api-bundle
* splitter-core-bundle

Quedando de la siguiente forma, por ejemplo para el primero {user}/splitter-project

## Subida del proyecto

* En nuestro workspace creamos un nuevo proyecto multiaplicación:

```bash
symfony-multiapp new:multi-app splitter
```

* Dentro del directorio splitter añadimos el repositorio de splitter-project y subimos, mediante:
```
git remote add origin ssh://git@bitbucket.org/jorgemarcial/splitter-project.git
git add .
git commit .
git push origin master
```

## Separar en multiples repositorios manteniendo el principal.

* Añadimos los repositorios remotos al repositorio principal.


### CmsBundle
```bash
git remote add splitter-cms-bundle-repo git@bitbucket.org:{user}/splitter-cms-bundle.git
git subtree split --prefix=src/CmsBundle -b splitter-cms-bundle
git push splitter-cms-bundle-repo splitter-cms-bundle:master
```

### ApiBundle
```bash
git remote add splitter-api-bundle-repo git@bitbucket.org:{user}/splitter-api-bundle.git
git subtree split --prefix=src/ApiBundle -b splitter-api-bundle
git push splitter-api-bundle-repo splitter-api-bundle:master
```

### CoreBundle
```bash
git remote add splitter-core-bundle-repo git@bitbucket.org:{user}/splitter-core-bundle.git
git subtree split --prefix=src/CmsBundle -b splitter-core-bundle
git push splitter-core-bundle-repo splitter-core-bundle:master
```

## Actualizar repositorios.

* Hacemos una modificación en el repositorio en CmsBundle\Controller\DefaultController añadiendo una linea nueva por ejemplo un comentario.
* Commit en rama master
* Push en rama master
* Actualizamos rama subtree:

```bash
git subtree split --prefix=src/CmsBundle -b splitter-cms-bundle
git push splitter-cms-bundle-repo splitter-cms-bundle:master
```

* Comprobamos si ambos repositorios están sincronizados.

## Finalmente podemos revisar algunos scripts, como por ejemplo el utilizado en SoapBundle, para automatizar esta gestión:

http://francis-besset.com/git-subtree-with-tags.html